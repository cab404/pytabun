# coding: utf-8
import json
from luna.abacus import gzip_json

__author__ = 'cab404'


class Translator:
    def translate(self, data):
        """
            Translates client's string to dict. Invoked at packet handling stream,
            so you can use cherrypy.request, cherrypy.response, etc.
        @param data: string to be converted.
        @raise NotImplemented: by default.
        """
        raise NotImplemented

    def build(self, data):
        """
            Creates string for client out of supplied dict. Invoked at packet handling stream,
            so you can use cherrypy.request, cherrypy.response, etc.
        @param data: dict to be built.
        @raise NotImplemented: by default.
        """
        raise NotImplemented


class JsonTranslator(Translator):
    """
        Simple realisation of Translator class, parses json and returns gzipped json.
        Automatically adds Content-Encoding: gzip
    """

    def build(self, data):
        return gzip_json(data)

    def translate(self, data):
        return json.loads(data)