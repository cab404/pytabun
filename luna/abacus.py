# coding: utf-8

import json
import cherrypy
from xmlrpclib import gzip_encode

import scrypt

import config


__author__ = 'cab404'


def gzip_json(dictionary):
    """
        Inserts gzip headers into CherryPy response, serializes given dict and gzip-s that string.
    @param dictionary: data to serialize
    @type dictionary: dict
    @return: gzipped json
    @rtype: str
    """
    cherrypy.response.headers["Content-Encoding"] = "gzip"
    return gzip_encode(json.dumps(dictionary))


def generate_crypto_hash(password):
    """
        Generates cryptohash using scrypt algorithm and salt from settings.
    @param password:
    """
    return scrypt.hash(password, config.password_salt)


def err(code, data=None):
    """
        Generates error packet.
    @param code:
    @return:
    """
    if not data:
        data = {}
    data["error"] = code
    return data