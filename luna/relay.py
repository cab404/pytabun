# coding: utf-8
import cherrypy
import time
import luna.translator as translator

__author__ = 'cab404'


class Relay:
    # Telling CherryPy to show this junction to the world.
    exposed = True

    # Translator.
    trans = None

    # All the handlers.
    _handlers = None

    # Log tag.
    tag = "METHOD_RELAY"

    def __init__(self, trans=None, tag="METHOD_RELAY"):
        """
        @param trans: Translator to be used while speaking to the client.
        @type trans translator.Translator
        """
        self.trans = trans
        self._handlers = {}
        self.tag = tag
        pass

    def register(self, handlers):
        """
            Registers handlers from dict. All entries should have form (str, class)
        @param handlers:
        @type handlers: dict
        """
        for arg in handlers.iteritems():
            cherrypy.log.error("Adding %s as handler class for method '%s'" % (str(arg[1]), arg[0]), context=self.tag)
            self._handlers[arg[0]] = arg[1]

    def GET(self):
        return self.trans.build({"error": -1})

    def POST(self):
        request = self.trans.translate(cherrypy.request.body.read())
        # Checking if method was supplied by client.
        if "method" in request:
            method_name = request["method"]

            # Checking if we can handle this method.
            if method_name in self._handlers:
                handler = self._handlers[method_name]

                return self.trans.build(handler().handle(request))

            else:
                return self.trans.build({"error": 2})  # Method not supported

        else:
            return self.trans.build({"error": 1})  # Method not supplied

