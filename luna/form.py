# coding: utf-8
from luna import db

__author__ = 'cab404'


class Form:
    """
        Superclass for utility classes/replicas for MongoDB entries.
        Handles collection naming, saving/fetching, initializing
        by ObjectId and primary keys.
    """
    _data_set = None
    """
        Data dictionary of this object.
        @type dict
    """
    _id = None
    """
        ObjectId in MongoDB.
        @type ObjectId
    """
    primary_key = None
    """
        Unique part for every object. Checked on update.
        @type dict
    """

    def __init__(self, ID=None):
        """
        @param ID: ObjectId. If not supplied then new data set will be created.
        @type ID: ObjectId
        """
        if ID:
            self._id = ID
            self.fetch_data_set()
        else:
            self._data_set = {}

    def collection(self):
        """
        @return: MongoDB collection for current class.
        @rtype: pymongo.collection.Collection
        """
        return db[str(self.__class__)]

    def fetch_data_set(self):
        """
            Synchronizes data set from DB using ObjectID.
        """
        self._data_set = self.collection().find_one(self._id)
        pass

    def get_data_set(self):
        """
            Well, returns _data_set.
        @return: pymongo.cursor.Cursor
        """
        return self._data_set

    def update_data_set(self):
        """
            Updates data set in DB with this object's collection.
            If primary key is supplied, then checks if collection has any
            entries, matching primary_key. If it finds one, then CloneEntry
            will be risen.
        """
        if self.primary_key:
            if self.collection().find_one(self.primary_key):
                raise DuplicateException

        if self._id:
            self.collection().update({"_id": self._id}, self.get_data_set())
        else:
            self._id = self.collection().insert(self.get_data_set())
        pass


class DuplicateException(Exception):
    """
        Risen if duplicate entry found.
    """
    pass