# coding: utf-8

__author__ = 'cab404'


class Handler:

    def handle(self, data):
        """
        @param data: Client's request.
        @raise NotImplemented: By default.
        @return Dictionary with return data.
        """
        raise NotImplemented