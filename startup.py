# coding: utf-8

__author__ = 'cab404'

if __name__ == '__main__':

    # Initializing MongoDB
    from pymongo import MongoClient
    import config
    import luna

    luna.db = MongoClient(config.db_url)[config.db_name]

    # Initializing handlers. We're doing this after everything db is already set up to
    # have no import problems in forms.
    from luna.translator import JsonTranslator
    from luna import relay
    import modules

    valve = relay.Relay(trans=JsonTranslator())
    valve.register(modules.active_handlers)

    # Configuring and booting CherryPy
    import cherrypy

    cherrypy.tree.mount(
        valve,
        '/',
        {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher()
            }
        }
    )

    cherrypy.engine.start()
    cherrypy.engine.block()