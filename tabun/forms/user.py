# coding: utf-8
from luna.form import Form
from luna.abacus import generate_crypto_hash
__author__ = 'cab404'


class User(Form):

    @property
    def primary_key(self):
        return {"name": self.get_name()}

    @property
    def get_password_hash(self):
        return self.get_data_set()["password_hash"]

    def set_password(self, password):
        self.get_data_set()["password_hash"] = generate_crypto_hash(password)
        pass

    def get_name(self):
        return self.get_data_set()["name"]