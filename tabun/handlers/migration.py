# coding: utf-8
from luna.form import DuplicateException
from tabun.forms.user import User
from luna.handler import Handler
import luna.abacus

__author__ = 'cab404'


class Migrate(Handler):

    def handle(self, data):
        user = User()

        user.get_data_set()["name"] = data["name"]

        try:
            user.update_data_set()
        except DuplicateException:
            return luna.abacus.err(5)

        return luna.abacus.err(0)
