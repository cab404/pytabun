# coding: utf-8
from tabun.handlers import migration

__author__ = 'cab404'

# Handler activation
active_handlers = {
    "migrate_to_equestria": migration.Migrate
}